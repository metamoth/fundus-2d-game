﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderScript : MonoBehaviour {

    public Color active;
    public Color inactive;

    // Use this for initialization
    void Start () {
        GetComponent<SpriteRenderer>().color = inactive;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void PlayerInteraction(PlayerState playerState)
    {
        playerState.SetMotionClimb();
        GetComponent<SpriteRenderer>().color = active;
    }
    private void StopPlayerInteraction(PlayerState playerState)
    {
        playerState.SetMotionWalk();
        GetComponent<SpriteRenderer>().color = inactive;
    }

    private void OnValidate()
    {
        GetComponent<SpriteRenderer>().color = inactive;
    }
}
