﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPolyCollider : MonoBehaviour {

    public bool showPolyCollider;

    private Vector2[] verts;
    private PolygonCollider2D pc2d;

	// Use this for initialization
	void Start () {
        verts = gameObject.GetComponent<PolygonCollider2D>().GetPath(0);        
	}
	
	// Update is called once per frame
	void OnDrawGizmos () {
		if (showPolyCollider)
        {
            Gizmos.color = new Color(1.0f, 1.0f, 0.0f, 0.1f);
            verts = gameObject.GetComponent<PolygonCollider2D>().GetPath(0);
            for (int i = 0; i < verts.Length - 1; i++)
            {
                Debug.DrawLine((Vector2)transform.position + verts[i] , (Vector2)transform.position + verts[i + 1]);
                Debug.DrawLine((Vector2)transform.position, (Vector2)transform.position + verts[i]);
            }

            Debug.DrawLine((Vector2)transform.position + verts[verts.Length-1], (Vector2)transform.position + verts[0]);
            Debug.DrawLine((Vector2)transform.position, (Vector2)transform.position + verts[verts.Length-1]);
        }
	}
}
