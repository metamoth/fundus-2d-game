﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour {

    public bool cameraZoom;
    public float zoomOut;
    public float zoomIn;
    public Vector3 cameraOffset = new Vector3(0f,0f,-16f);
    public float animTime = 500;

    public Color LightsOnColor;
    public Color LightsOffColor;

    private LightController Lights;
    private Camera Cam;
    private GameObject player;
    private PlayerState playerState;
    private float timePassed = 1.0f;


    //Uitlity
    float Func(float t)
    {
        List<float> p = new List<float>
        {
            1.000f,
            0.25f,
            0.625f,
            0.005f
        };
        float f = (1 - t) * (1 - t) * (1 - t) * p[0] + 3 * t * (1 - t) * (1 - t) * p[1] + 3 * t * t * (1 - t) * p[2] + t * t * t * p[3];
        return (f);
    }


	// Use this for initialization
	void Start () {
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        Cam = gameObject.GetComponent<Camera>();
        player = GameObject.Find("Player");
        playerState = player.GetComponent<PlayerState>();

        if (playerState.inLightArea && Lights.areOn)
		{
            Cam.backgroundColor = LightsOnColor;
        }
        else
        {

            Cam.backgroundColor = LightsOffColor;

        }
        timePassed = 0f;

        transform.position = player.transform.position - transform.position + cameraOffset;
        transform.position = new Vector3(transform.position.x, transform.position.y, -16f);
    }
	
	// Update is called once per frame
	void Update () {
        
        if (playerState.inLightArea && Lights.areOn)
        {
            timePassed -= Time.deltaTime;
            Cam.backgroundColor = LightsOnColor;
        }
        else
        {

            timePassed += Time.deltaTime * 5;
            Cam.backgroundColor = LightsOffColor;

        }


        if (timePassed / animTime < 0) timePassed = 0;
        if (timePassed / animTime > 1) timePassed = animTime;

        if (cameraZoom)
        {
            Cam.orthographicSize = timePassed / animTime * (zoomIn - zoomOut) + zoomOut;
        }
        else
        {
            Cam.orthographicSize = zoomOut;
        }
        
        transform.position = transform.position + 0.01f*(player.transform.position - transform.position) + cameraOffset;
        transform.position = new Vector3(transform.position.x, transform.position.y, -16f);

    }
}
