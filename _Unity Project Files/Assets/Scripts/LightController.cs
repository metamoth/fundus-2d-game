﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{


    [Header("LIGHTS")]
    [Tooltip("Die wichtigste Variable im Spiel! Ist das Licht an oder aus?")]
    public bool areOn = false;
    [Space(5)]
    [Tooltip("Die Maximale Zeit in Sekunden, die zwischen 2 Switches erfolgen darf, um timesSwitched hochzuzählen")]
    public float maxFlickerTime = 0.2f;
    [Tooltip("Wie oft man hin und her flickern muss, um die Flicker Aktion auszulösen")]
    public int enoughTimes = 5;

    //Utility
    private bool buttonUp = true;
    private List<LightSocketScript> lightSockets;
    private List<LightSocketScript> electroSockets;


    //Flickering Variablen
    private bool startCounting = false;
    private float countSinceSwitch = 0.0f;
    private int timesSwitched = 0;
    private Transform dynEnv;
    private Transform allElectroSockets;
    private Transform allLightSockets;
    private PlayerState playerState;
    private GameObject player;
    private WhiteGuyScript whiteGuyScript;
    private GameObject spotlight;

    public bool GetPower()
    {
        return this.areOn;
    }

    // Use this for initialization
    void Start()
    {
        //whiteGuyScript = GameObject.Find("WhiteGuy").GetComponent<WhiteGuyScript>();
        spotlight = GameObject.Find("Spotlight");
        allElectroSockets = GameObject.Find("AllElectroSockets").transform;        
        allLightSockets = GameObject.Find("AllLightSockets").transform;
        dynEnv = GameObject.Find("Dynamic Environment").transform;
        player = GameObject.Find("Player");
        playerState = player.GetComponent<PlayerState>();

        lightSockets = new List<LightSocketScript>();
        Transform socketGroup = GameObject.Find("AllLightSockets").transform;

        foreach(Transform lightSocketT in allLightSockets)
        {
            lightSockets.Add(lightSocketT.GetComponent<LightSocketScript>());
        }

        UpdateLight();
    }

    // Update is called once per frame
    void Update()
    {

        int winCounter = 0;
        foreach(LightSocketScript socket in lightSockets)
        {
            if(socket.bulbInside != null)
                if(socket.bulbInside.GetComponent<BulbController>().isOn)
                    winCounter++;
        }
        //WIN CONDITION
        if (winCounter == lightSockets.Count)
        {
            print(winCounter.ToString() + " / " + lightSockets.Count.ToString());
            GameObject.Find("Game Manager").SendMessage("Win");
        }

        if (Input.GetAxis("Switch") == 0) buttonUp = true;
        if (Input.GetAxis("Switch") == 1 && buttonUp)
        {
            buttonUp = false;
            Switch();
        }

        if (startCounting)
        {
            countSinceSwitch += Time.deltaTime;
        }

        if (countSinceSwitch >= maxFlickerTime)
        {
            countSinceSwitch = 0;
            startCounting = false;
            timesSwitched = 0;
        }


    }


    void Switch()
    {
        startCounting = true;

        if (countSinceSwitch < maxFlickerTime)
        {
            countSinceSwitch = 0;
            timesSwitched++;
        }

        if (timesSwitched >= enoughTimes)
        {
            print("Flicker");
            Flicker();
        }

        areOn = !areOn;
        UpdateLight();


    }

    void Flicker()
    {

        for (int i = 0; i < GameObject.FindGameObjectsWithTag("Enemy").Length; i++)
        {
            GameObject.FindGameObjectsWithTag("Enemy")[i].SendMessage("GoAway");
        }

        //GameObject.Find("WhiteGuy").SendMessage("Teleport");

        countSinceSwitch = 0.0f;
        startCounting = false;
        timesSwitched = 0;

    }

    void UpdateLight()
    {

        //spotlight.GetComponent<SpriteRenderer>().enabled = !areOn;

        if (areOn)
        {
            GameObject.Find("LightDisplay").GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            for (int i = 0; i < lightSockets.Count; i++)
            {
                if (lightSockets[i].global)
                {
                    lightSockets[i].TurnOn();
                }
            }

            for (int i = 0; i < dynEnv.childCount; i++)
            {
                GameObject box = dynEnv.GetChild(i).gameObject;
                EnvSwitchScript boxScript = box.GetComponent<EnvSwitchScript>();
                box.SetActive(boxScript.visibileInDark);
            }

            playerState.inLightArea = false;
            //whiteGuyScript.inLightArea = false;
        }
        else
        {
            GameObject.Find("LightDisplay").GetComponent<SpriteRenderer>().color = new Color(0 , 0, 0, 0);

            for (int i = 0; i < lightSockets.Count; i++)
            {
                lightSockets[i].TurnOff();
            }
        }
        Power();


    }

    public void Power()
    {

        foreach (Transform electroSocket in allElectroSockets)
        {
            if (electroSocket.gameObject.GetComponent<ElectroSocketScript>().socketMode == ElectroSocketScript.SocketMode.G_OUT)
                electroSocket.SendMessage("Power", areOn);
        }
    }
}
