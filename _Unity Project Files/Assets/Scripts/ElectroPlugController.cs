﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroPlugController : MonoBehaviour
{

    private GameObject socket;

    private SpriteRenderer spriteRenderer;

    //UNITY START
    private void Start()
    {
   
    }
    //UNITY UPDATE
    private void Update()
    {

    }

    public GameObject GetOtherPlug()
    {
        foreach(Transform ct in transform.parent)
        {
            if(ct.gameObject != gameObject && ct.gameObject.GetComponent<ElectroPlugController>() != null)
            {
                return ct.gameObject;
            }
        }
        return null;
    }

    public GameObject GetSocket()
    {
        if(socket != null)
        {
            return this.socket;
        }
        else
        {
            return null;
        }
    }

    public void SetSocket(GameObject s)
    {
        this.socket = s;
    }
    public GameObject GetOtherSocket()
    {
        GameObject plug = GetOtherPlug();
        if(plug != null)
        {
            return plug.GetComponent<ElectroPlugController>().GetSocket();
        }
        else
        {
            return null;
        }
    }

    


    
}
