﻿using UnityEngine;
using System.Collections;

public class PerPixelCollision
{

    private bool collision = false;
    public bool IsCollision()
    {
        return collision;
    }

    private PixelCollider a;
    private PixelCollider b;

    Bounds2D boundsA;
    Bounds2D boundsB;

    private Color[] bitsA;
    private Color[] bitsB;

    public static bool Collides(PixelCollider a, PixelCollider b)
    {
        return new PerPixelCollision(a, b).collision;
    }

    private PerPixelCollision(PixelCollider a, PixelCollider b)
    {
        this.a = a;
        this.b = b;
        boundsA = a.GetBounds();
        boundsB = b.GetBounds();
        CheckCollsion();
    }

    private void CheckCollsion()
    {
        if (TexturesOverlap())
        {
            collision = PixelCollision();
        }
    }

    private bool TexturesOverlap()
    {
        return a.GetBounds().Overlaps(b.GetBounds());
    }

    private bool PixelCollision()
    {
        GetBits();
        if (bitsA.Length != bitsB.Length)
        {
            Debug.LogError("Bits do not overlap");
            return false;
        }
        for (int i = 0; i < bitsA.Length; i++)
        {
            // If both colors are not transparent (the alpha channel is not 0), then there is a collision
            if (bitsA[i].a != 0 && bitsB[i].a != 0)
            {
                return true;
            }
        }
        return false;
    }

    private void GetBits()
    {
        //Intersection of the two bounds in global space.
        Bounds2D intersection = boundsA.Intersection(boundsB);

        //Intersections in local space.
        Bounds2D intersectionA = boundsA.ToLocal(intersection);
        Bounds2D intersectionB = boundsB.ToLocal(intersection);

        bitsA = GetBitArray(intersectionA, a.collisionMask);
        bitsB = GetBitArray(intersectionB, b.collisionMask);
    }

    private static Color[] GetBitArray(Bounds2D section, Texture2D texture)
    {
        return texture.GetPixels(section.X, section.Y, section.Width, section.Height);
    }

}