﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Snapshot))]
public class SnapshotEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Snapshot myScript = (Snapshot)target;
        if (GUILayout.Button("Capture"))
        {
            myScript.Capture();
        }
    }
}