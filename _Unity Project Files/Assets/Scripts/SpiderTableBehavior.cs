﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderTableBehavior : MonoBehaviour
{
    ///////////////////////////////   CLASS DECLARATION   ///////////////////////////////////////////////////////////

    public enum NextAction { transformToSpider, transformToTable, standUp, walk};
    public enum Motion { stay, walk, stalk}

    //PUBLIC VARIABLES - FOR EXTERNEAL PROTOTYPING
    [Header("States")]
    public NextAction nextAction;
    public Motion motion;

    [Header("Walking")]
    public float walkSpeed = 500.0f;
    public Int16 walkDirection = 1;
    public float stepAngle = 45f;
    public float stepTime = 2f;
    public float walkLiftSpeed = 1f;
    public float walkStretchSpeed = 1f;
    public LayerMask stepingTargetMask;
    public float gravityScale = 0.1f;
    public float minGravityDist = 1f;
    public bool stickToGroundGravity = false;

    [Header("Attacking")]
    public float attackSpeed = 1.0f;
    public float attackDistance = 0.1f;

    [Header("Lifting")]
    public float liftSpeed = 1.0f;
    public float legHeight = 1.0f;
    public float platformScale = 1.0f;

    [Header("Actor State")]
    public bool inLight = false;
    




    //PRIVATE VARIABLES - FOR INTERNAL UTILITY
    private GameObject Player;
    private GameObject Platform;
    private GameObject Shadow;

    private List<GameObject> light_only;
    private List<GameObject> dark_only;
    private List<GameObject> legs_spider;
    private List<Transform> legs_spider_end;
    private List<GameObject> legs_normal;

    //Animation
    private bool initAnim;
    private float stepTimePassed = 0f;
    private bool pushForward = true;
    private bool legStreched = false;
    private bool legLifted = false;


    private AnimationState animState;
    private LightController Lights;
    private Rigidbody2D rb2d;

    private List<float> initLegHeights;
    private List<float> initLegOffset;
    private float initPlatformWidth;
    private float initShadowWidth;

    private float dir = 1f;

    private Vector2 gravity = new Vector2(0, 0);


   

   

   
    
    //Utility
    bool DoubleListed(List<GameObject> list,GameObject new_element)
    {
        foreach(GameObject exisiting_element in list)
        {
            if(exisiting_element.name == new_element.name)
            {
                return true;
            }
        }
        return false;
    }


    ///////////////////////////////   UNITY ENGINE CALLS   ///////////////////////////////////////////////////////////
    // Use this for object related initialization
    private void Init()
    {
        motion = Motion.stay;
        //get external game objects
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        Player = GameObject.Find("Player");
        Platform = transform.Find("Platform").gameObject;
        Shadow = transform.Find("+TableShadow").gameObject;

        rb2d = gameObject.GetComponent<Rigidbody2D>();

        light_only = new List<GameObject>();
        dark_only = new List<GameObject>();
        legs_spider = new List<GameObject>();
        legs_spider_end = new List<Transform>();
        legs_normal = new List<GameObject>();
        initLegHeights = new List<float>();
        initLegOffset = new List<float>();

        //get swapable gameobjects
        UpdateChildren();
        UpdateCustomTransform();
    }

   
    private void OnEnable() // in editor
    {
        Init();
    }
    void Start() // in game
    {
        Init();
    }


    // Update is called once per frame
    void Update()
    {
        //PRESS ENTER TO UPDATE LIMB CHANGES
        if (Input.GetKey(KeyCode.Return))
        {
            Debug.Log("updated childen");
            UpdateChildren();
        }

        //UPDATE ANIMATION STATE
        UpdateAnimation();
        UpdateCustomTransform();
        UpdateGravity();
    }

    ////////////////////////////////   CUSTOM TRANSFORM   ///////////////////////////////////////////////////////////////////////////////////////////////////

    void UpdateCustomTransform()
    {
        //update spiderLegs (offset, scale)
        for(int i = 0; i < legs_spider.Count; i++)
        {
            legs_spider[i].transform.localPosition = new Vector3(initLegOffset[i] * platformScale, legs_spider[i].transform.localPosition.y, 0);
            legs_spider[i].transform.localScale = new Vector3(legs_spider[i].transform.localScale.x, initLegHeights[i] * legHeight, 0);
        }
        //update normalLegs (offset)
        for (int i = 0; i < legs_normal.Count; i++)
        {
            legs_normal[i].transform.localPosition = new Vector3(initLegOffset[i] * platformScale, legs_normal[i].transform.localPosition.y, 0);
        }

        Platform.transform.localScale = new Vector3(initPlatformWidth * platformScale, Platform.transform.localScale.y, 0);
        Shadow.transform.localScale = new Vector3(initShadowWidth * platformScale, Shadow.transform.localScale.y, 0);

    }

    ////////////////////////////////   CUSTOM LIMBS   ///////////////////////////////////////////////////////////////////////////////////////////////////
    void UpdateChildren()
    {
        foreach (Transform child_tramsform in transform)
        {
            GameObject child = child_tramsform.gameObject;

            if (child.name[0] == '+' && !DoubleListed(light_only, child)) // light only
            {
                light_only.Add(child);
                if (child.name[1] == 'L' && !DoubleListed(legs_normal, child))
                {
                    legs_normal.Add(child);
                }
            }
            else if (child.name[0] == '-' && !DoubleListed(light_only, child)) // dark only
            {
                dark_only.Add(child);

                if (child.name[1] == 'L' && !DoubleListed(legs_spider, child))
                {
                    legs_spider.Add(child);
                    legs_spider_end.Add(child_tramsform.Find("Armature").Find("Bone").Find("Bone_001").Find("Bone_002"));
                    initLegHeights.Add(child_tramsform.localScale.y);
                    initLegOffset.Add(child_tramsform.localPosition.x);
                }
            }
            
            initShadowWidth = Shadow.transform.localScale.x;
            initPlatformWidth = Platform.transform.localScale.x;
            
        }

        UpdateLightState(Lights.areOn && inLight);
    }


    ////////////////////////////////   ANIMATION CONTROLL   ///////////////////////////////////////////////////////////
    void UpdateAnimation()
    {
        if (legs_spider[0].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.1f)
        {
            initAnim = false;
            switch (nextAction)
            {
                case NextAction.transformToSpider:
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    TransformToSpider();
                    nextAction = NextAction.standUp;                    
                    break;

                case NextAction.transformToTable:
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    TransfromToTable();
                    nextAction = NextAction.transformToSpider;
                    break;

                case NextAction.standUp:
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    StandUp();
                    nextAction =NextAction.walk;
                    break;

                case NextAction.walk:
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    motion = Motion.walk;
                    break;

            }

            switch (motion)
            {
                case Motion.stay:
                    break;
                case Motion.walk:
                    Walk(walkDirection);
                    break;
            }
        }
    }


    ////////////////////////////////   ACTIONS   ///////////////////////////////////////////////////////////

    void TransformToSpider()
    {
        initAnim = false;
        nextAction = NextAction.standUp;

        //Player animation for all legs
        foreach (GameObject leg in legs_spider)
        {
            leg.GetComponent<Animator>().Play("TransformToSpider", 0, 0);
        }
    }

    void TransfromToTable()
    {
        foreach (GameObject leg in legs_spider)
        {
            leg.GetComponent<Animator>().Play("TransformToTable", 0, 0);
        }
        stepTimePassed = 0;
        rb2d.bodyType = RigidbodyType2D.Dynamic;
        motion = Motion.stay;
    }

    void StandUp()
    {
        foreach (GameObject leg in legs_spider)
        {
            leg.GetComponent<Animator>().Play("StandUp", 0, 0);
        }
    }

    void Turn()
    {
        Debug.Log("Turn");
        dir *= -1f;
        Walk(walkDirection);
    }

    void Walk(int direction)
    {

        //float animPhase = stepTimePassed / stepTime;
        ////manage Animation
        //if (animPhase < -0.8 && pushForward && !legLifted)
        //{       
        //    //lift leg on start
        //    foreach (GameObject leg in legs_spider)
        //    {
        //        if (direction > 0 && !leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
        //        {
        //            legLifted = true;
        //            leg.GetComponent<Animator>().Play("LiftLeg");
        //        }
        //        else if (direction < 0 && leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
        //        {
        //            legLifted = true;
        //            leg.GetComponent<Animator>().Play("LiftLeg");
        //        }
        //    }
        //}
        

        //if(animPhase > 0.5 && pushForward && !legStreched)
        //{
        //    foreach (GameObject leg in legs_spider)
        //    {
        //        if (direction > 0 && !leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
        //        {
        //            legStreched = true;
        //            leg.GetComponent<Animator>().Play("StrechLeg");
        //        }
        //        else if (direction < 0 && leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
        //        {
        //            legStreched = true;
        //            leg.GetComponent<Animator>().Play("StrechLeg");
        //        }
        //    }

        //}
        ////manage step phase

        //if (animPhase <= -1)
        //{
        //    pushForward = true;
        //    legLifted = false;
        //}


        //if (animPhase >= 1)
        //{
        //    pushForward = false;
        //    legStreched = false;
        //}
        //if (pushForward)
        //{
        //    stepTimePassed += Time.deltaTime * walkLiftSpeed;
        //}
        //else
        //{
        //    stepTimePassed -= Time.deltaTime * walkStretchSpeed;
        //}


        ////manage step metrics
        //for(int i = 0; i < legs_spider.Count; i++)
        //{

        //    Vector2 localDown = Rotate(Vector2.down, transform.localEulerAngles.z);
        //    Vector2 localNext = Rotate(localDown, direction*stepAngle);

        //    RaycastHit2D nextTarget = Physics2D.Raycast(legs_spider[i].transform.position, localNext, float.PositiveInfinity, stepingTargetMask);

        //    Debug.DrawLine(legs_spider[i].transform.position, legs_spider_end[i].position, Color.magenta);
        //    Debug.DrawLine(legs_spider[i].transform.position, nextTarget.point, Color.blue);

        //    if(((Vector2)legs_spider[i].transform.position - nextTarget.point).magnitude <= initLegHeights[i] * 5f) //if in reach then setp
        //    {
        //        if (legs_spider[i].GetComponent<LimbController_SpiderLeg>().leftLeg)
        //        {
        //            legs_spider[i].transform.localEulerAngles = new Vector3(0, 0, -stepAngle * animPhase);
        //        }
        //        else
        //        {
        //            legs_spider[i].transform.localEulerAngles = new Vector3(0, 0, stepAngle * stepTimePassed / stepTime);    
        //        }
                
        //    }

        //}
        
        
    }

    ////////////////////////////////   UPDATE LIGHT STATE   ///////////////////////////////////////////////////////////
    private void UpdateLightState(bool light)
    {
        inLight = light;

        foreach (GameObject obj in light_only) // activate objs only visible in light
        {
            if(obj.activeSelf != light) obj.SetActive(light);
        }
        foreach (GameObject obj in dark_only) // deactivate objs only visible in dark
        {
            if (!obj.activeSelf != !light) obj.SetActive(!light);
        }

        Platform.GetComponent<SpriteRenderer>().color = inLight ? Color.white : Color.black;

        if (light)
        {
            TransfromToTable();
        }
        else
        {
            TransformToSpider();
        }

    }

    private void UpdateLightState()
    {

        foreach (GameObject obj in light_only) // activate objs only visible in light
        {
            obj.SetActive(inLight);
        }
        foreach (GameObject obj in dark_only) // deactivate objs only visible in dark
        {
            obj.SetActive(inLight);
        }

        Platform.GetComponent<SpriteRenderer>().color = inLight ? Color.white : Color.black;

        if (inLight)
        {
            TransfromToTable();
        }
        else
        {
            TransformToSpider();
        }

    }

    private void UpdateGravity()
    {
        if (stickToGroundGravity)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Rotate(Vector2.down, transform.eulerAngles.z), float.PositiveInfinity, stepingTargetMask);
            Vector2 relPos = (hit.point - (Vector2)transform.position);
            if (relPos.magnitude <= minGravityDist)
            {
                gravity = -hit.normal * 1500f * gravityScale;
            }
            Debug.DrawLine(transform.position, (Vector2)transform.position + relPos.normalized * minGravityDist, Color.grey);
            rb2d.AddForce(gravity);
        }
        
    }

    ////////////////////////////////   TRIGGER FUNCTIONS   ///////////////////////////////////////////////////////////

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Light_Source")
        {
            UpdateLightState(true);
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Light_Source")
        {
            UpdateLightState(false);
        }
    }

    ////////////////////////////////   UTILITY FUNCTIONS   ///////////////////////////////////////////////////////////

    Vector2 Rotate(Vector2 v, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(angle) * v.x - Mathf.Sin(angle) * v.y, Mathf.Sin(angle) * v.x + Mathf.Cos(angle) * v.y);
    }

}



