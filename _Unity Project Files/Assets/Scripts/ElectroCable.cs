﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroCable : MonoBehaviour {

    public Color cableColor;

    private List<Transform> cableSegments;
    public GameObject PlugA;
    public GameObject PlugB;
    public GameObject SocketA;
    public GameObject SocketB;
 
    public bool power = false;

    public bool hideEditorLine = false;

    // Use this for initialization
    void Start () {
        cableSegments = new List<Transform>();
        UpdateCableSegments();
        hideEditorLine = true;
    }
    


    // Update is called once per frame
    void Update () {
        //update line
        cableSegments[0].position = PlugA.transform.position;
        cableSegments[cableSegments.Count-1].position = PlugB.transform.position;
        for (int i = 0; i < cableSegments.Count - 1; i++)
        {
            Debug.DrawLine(cableSegments[i].position, cableSegments[i + 1].position, cableColor);
        }

    }

    private void UpdateCableSegments()
    {
        cableSegments.Clear();
        foreach (Transform seg in gameObject.transform)
        {
            cableSegments.Add(seg);
        }
    }
 
    private void OnDrawGizmos()
    {
        if (!hideEditorLine)
        {
            Gizmos.color = cableColor;
            Gizmos.DrawLine(PlugA.transform.position, PlugB.transform.position);
        }
            
    }

    private void OnValidate()
    {
        PlugA.GetComponent<SpriteRenderer>().color = cableColor;
        PlugB.GetComponent<SpriteRenderer>().color = cableColor;
    }
}
