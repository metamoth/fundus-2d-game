﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public enum Motion {catchUp, approach, attack, stay};


    [Header("Motion")]
    public bool overrideState = false;
    public Motion motion = Motion.catchUp;
	public float catchUpSpeed    = 1.2f;
    public float approachSpeed   = 0.4f;
    public float attackSpeed     = 1.5f;
    public float toEyeLevelSpeed = 0.1f;
    public bool inLightArea = false;


	[Tooltip("Der Faktor, um den der Gegner beim Flickern weggestoßen wird")]
	public float pushForce = 5.0f;

    [Header("Behavior")]
    public float catchUpDistance =  1.0f;
    public float approachDistance = 1.0f;
    public float attackDistance =   1.0f;
    [Space(5)]
    public float minAttackWaitTime = 1.0f;
    public float maxAttackWaitTime = 1.0f;

    [Header("Analysis")]
    public bool showBehaviorCircles = false;

    private Rigidbody2D rb2d;
	private GameObject Player;
    private LightController Lights;




    // Use this for initialization
    void Start () {
        rb2d = this.GetComponent<Rigidbody2D> ();
		Player = GameObject.Find ("Player");
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
	}
	
	// Update is called once per frame
	void Update () {

		if (Lights.GetComponent<LightController> ().areOn) {
			this.gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		}
        else
        {
			this.gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		}

        inLightArea = gameObject.GetComponent<EnvSwitchScript>().inLightArea;

        if (inLightArea && Lights.areOn)
        {

            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;

        }
        else
        {
            rb2d.constraints = RigidbodyConstraints2D.None;
            if (!overrideState)
            {
                Sence(Player);
            }

            Seek(Player);
        }


    }



    //Immer für Physikberechnungen benutzen!
    void FixedUpdate()
	{
		
  
    }


	// simple Bewegung. berücksichtigt weder Gravitation noch Sprünge usw.

	void Seek(GameObject target)
	{

        Vector2 toTargetPosition = ((Vector2)target.transform.position - (Vector2)this.gameObject.transform.position).normalized;
        Vector2 toTargetHeight = new Vector2(0, target.transform.position.y - transform.position.y);

        if (motion == Motion.catchUp)
        {
            rb2d.velocity = toTargetPosition * catchUpSpeed;
        }

        else if (motion == Motion.approach)
        {
            rb2d.velocity = toTargetPosition  * approachSpeed + toTargetHeight * toEyeLevelSpeed * Time.deltaTime;
        }

        else if (motion == Motion.attack)
        {
            rb2d.velocity = toTargetPosition * attackSpeed;
        }
        else
        {
            rb2d.velocity = Vector2.zero;
        }

        //Update Direction
        gameObject.transform.LookAt(target.transform,Vector3.back);
	}

    void Sence(GameObject target)
    {

        float distanceToTarget = (transform.position - target.transform.position).magnitude;

        if (distanceToTarget > catchUpDistance)
        {
            motion = Motion.catchUp;
        }

        else if (distanceToTarget > approachDistance)
        {
            motion = Motion.approach;
        }

        else if (distanceToTarget <= attackDistance)
        {
            motion = Motion.attack;
        }

    }
    private void OnDrawGizmos()
    {
        if (showBehaviorCircles)
        {
            Gizmos.color = new Color(0, 0, 1, .1f);
            Gizmos.DrawSphere(transform.position, catchUpDistance);

            Gizmos.color = new Color(1, 1, 0, .1f);
            Gizmos.DrawSphere(transform.position, approachDistance);

            Gizmos.color = new Color(1f, 0, 0, .1f);
            Gizmos.DrawSphere(transform.position, attackDistance);
        }
    }


	void GoAway()
	{

		rb2d.AddForce ((this.transform.position - Player.transform.position).normalized * pushForce);

    }
    /////////////////////////////////////  TRIGGER HANDLING  //////////////////////////////////////////////////////////////////////////////

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Light_Source")
        {
            inLightArea = true;
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Light_Source")
        {
            inLightArea = false;
        }
    }


}

