﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroSocketScript : MonoBehaviour
{
    public enum SocketMode { G_OUT, S_OUT, S_IN, O_IN};

    public Color colG_OUT;
    public Color colS_OUT;
    public Color colS_IN;
    public Color colO_IN;

    public SocketMode socketMode;

    public GameObject energySource;
    public GameObject energyReciever;
    public GameObject plugInside = null;
    public Vector2 plugOffset;
    

    private PlayerActions playerActions;
    private PlayerState playerState;
    private LightController Lights;
    private PolygonCollider2D lightArea;
    private Transform dynEnv;
    private bool power;

    //UTILITY
  
    // Use this for initialization
    void Start()
    {
        playerActions = GameObject.Find("Player").GetComponent<PlayerActions>();
        playerState = GameObject.Find("Player").GetComponent<PlayerState>();
        //        whiteGuyScript = GameObject.Find("WhiteGuy").GetComponent<WhiteGuyScript>();
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        dynEnv = GameObject.Find("Dynamic Environment").transform;
    }






    /////////////////////////////// CABLE MANAGEMENT ///////////////////////////////////////////
    
    private void PlugIn(GameObject plug)
    {
        //plug physics
        plug.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        plug.transform.position = (Vector2)transform.position + plugOffset;

        //plug exchange
        playerActions.pickUp = null;
        plugInside = plug;
        plugInside.GetComponent<ElectroPlugController>().SetSocket(gameObject);
        plugInside.GetComponent<SpriteRenderer>().sortingLayerName = "Background";


        switch (socketMode)
        {
            case SocketMode.G_OUT:
                if(plug.GetComponent<ElectroPlugController>().GetOtherSocket() != null)
                {
                    energyReciever = plug.GetComponent<ElectroPlugController>().GetOtherSocket();
                    energyReciever.GetComponent<ElectroSocketScript>().energySource = gameObject;
                }
                break;
            case SocketMode.S_OUT:
                if (plug.GetComponent<ElectroPlugController>().GetOtherSocket() != null)
                {
                    energyReciever = plug.GetComponent<ElectroPlugController>().GetOtherSocket();
                    energyReciever.GetComponent<ElectroSocketScript>().energySource = gameObject;
                }
                break;
            case SocketMode.S_IN:
                if (plug.GetComponent<ElectroPlugController>().GetOtherSocket() != null)
                {
                    energySource = plug.GetComponent<ElectroPlugController>().GetOtherSocket();
                    energySource.GetComponent<ElectroSocketScript>().energyReciever = gameObject;
                }
                break;
            case SocketMode.O_IN:
                if (plug.GetComponent<ElectroPlugController>().GetOtherSocket() != null)
                {
                    energySource = plug.GetComponent<ElectroPlugController>().GetOtherSocket();
                    energySource.GetComponent<ElectroSocketScript>().energyReciever = gameObject;
                }
            
                break;
        }

        Lights.Power();

    }

    public void PullOut()
    {
        Power(false);
        switch (socketMode)
        {
            case SocketMode.G_OUT:
                if (energyReciever)
                {
                    energyReciever.GetComponent<ElectroSocketScript>().energySource = null;
                    energyReciever = null;
                }
                break;

            case SocketMode.S_OUT:
                if (energyReciever)
                {
                    energyReciever.GetComponent<ElectroSocketScript>().energySource = null;
                    energyReciever = null;
                }
                break;

            case SocketMode.S_IN:
                if (energySource)
                {
                    energySource.GetComponent<ElectroSocketScript>().energyReciever = null;
                    energySource = null;
                }
                break;
            case SocketMode.O_IN:
                if (energySource)
                {
                    energySource.GetComponent<ElectroSocketScript>().energyReciever = null;
                    energySource = null;
                }
                break;
        }
        plugInside.GetComponent<SpriteRenderer>().sortingLayerName = "ForeGround";
        plugInside.GetComponent<ElectroPlugController>().SetSocket(null);
        plugInside = null;
    }

    public void Power(bool power)
    {
        if(energyReciever != null)
        {
            energyReciever.SendMessage("Power", power);
        }
    }


    /////////////////////////////// TRIGGGER ///////////////////////////////////////////
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (plugInside == null)
        {
            if(coll.transform.parent != null)
                if (coll.transform.parent.gameObject.tag == "Electro_Plug")
                    if (coll.transform.parent.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
                    {
                        PlugIn(coll.transform.parent.gameObject);
                    }
        }
            
    }



    /////////////////////////////// EDITOR ///////////////////////////////////////////
    private void OnValidate()
    {
        try
        {

            TextMesh textMesh = transform.FindChild("Text").GetComponent<TextMesh>();
            switch (socketMode)
            {
                case SocketMode.G_OUT:
                    gameObject.GetComponent<SpriteRenderer>().color = colG_OUT;
                    textMesh.text = "G_OUT";
                    break;

                case SocketMode.S_OUT:
                    gameObject.GetComponent<SpriteRenderer>().color = colS_OUT;
                    textMesh.text = "S_OUT";
                    break;

                case SocketMode.S_IN:
                    gameObject.GetComponent<SpriteRenderer>().color = colS_IN;
                    textMesh.text = "S_IN";
                    break;

                case SocketMode.O_IN:
                    gameObject.GetComponent<SpriteRenderer>().color = colO_IN;
                    textMesh.text = "O_IN";
                    break;
            }
        }
        catch (Exception e)
        {
            print("VAL ERR");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + plugOffset);
    }


}

