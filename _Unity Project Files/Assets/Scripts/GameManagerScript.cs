﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

    private GameObject canvas;
    public bool gamePaused;

    private bool buttonWasUp = true;

	// Use this for initialization
	void Start () {
        canvas = Camera.main.transform.Find("Canvas").gameObject;
        canvas.SetActive(true); //avoids accidental selection of canvas outside the game, only turned on in game
        gamePaused = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonUp("Start")) buttonWasUp = true;
        if(Input.GetButtonDown("Start") && buttonWasUp)
        {
            buttonWasUp = false;
            TogglePause();
        }
		
	}
    void Win()
    {
        GameObject.Find("GUI").GetComponent<GUIManager>().derText.color = Color.green;
        GameObject.Find("GUI").GetComponent<GUIManager>().textString = "YOU WIN";
    }

    void TogglePause()
    {
        gamePaused = !gamePaused;
        if (gamePaused)
        {
            GameObject.Find("GUI").GetComponent<GUIManager>().derText.color = Color.blue;
            GameObject.Find("GUI").GetComponent<GUIManager>().textString = " - paused - ";
            Time.timeScale = 0;
        }
        else
        {
            GameObject.Find("GUI").GetComponent<GUIManager>().textString = "";
            Time.timeScale = 1;
        }

    }
}
