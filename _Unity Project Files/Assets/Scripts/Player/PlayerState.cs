﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {

    public enum Local { onGround, inAir, attached };
    public enum Motion { walk, crouch, stay, climb };
    
    [Header("Player State")]
    [Tooltip("controls available moves, for a given spacial situation")]
    public Local local;
    [Tooltip("controls how the player is able to move")]
    public Motion motion;
    public bool inLightArea = false;
    public int direction = 1;

    [Header("Player Health")]
    public bool invincible = true;
    public float maxHP = 100.0f;
    public float currentHP;
    public float whiteGuyDamage = 100.0f;
    public float defaultDamage = 5.0f;

	private Rigidbody2D rb2d;
	private PlayerState playerState;

    //Utility
    private Vector2 Rotate(Vector2 v, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(angle) * v.x - Mathf.Sin(angle) * v.y, Mathf.Sin(angle) * v.x + Mathf.Cos(angle) * v.y);
    }

    private float BoxAngle(Transform target, Vector2 contactPoint)
    {
        float angle = 0;
        float theta =  target.eulerAngles.z;
        Vector2 relVector = transform.position - target.position;
        Vector2 targetNormal = new Vector2(Mathf.Cos(theta) * relVector.x * target.localScale.x - Mathf.Sin(theta) * relVector.y * target.localScale.y, Mathf.Sin(theta) * relVector.x / -target.localScale.x + Mathf.Cos(theta) * relVector.y * target.localScale.y).normalized;
        angle = Vector2.Angle(relVector, targetNormal);
        return angle;
    }


    // Use this for initialization
    void Start () {
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		playerState = gameObject.GetComponent<PlayerState> ();
        currentHP = maxHP;
        local = Local.inAir;
        motion = Motion.walk;
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerStay2D(Collider2D coll)
    {

        if (!invincible)
        {
            if (coll.gameObject.tag == "Enemy")
            {
                currentHP -= Time.deltaTime * defaultDamage;
            }
            else if (coll.gameObject.tag == "WhiteGuy")
            {
                currentHP -= Time.deltaTime * whiteGuyDamage;
            }
            if (currentHP <= 0)
            {
                Die();
            }
        }
    }

    //private void OnTriggerExit2D(Collider2D coll)
    //{
    //    if (coll.gameObject.tag == "Light_Source")
    //    {
    //        inLightArea = false;
    //    }

    //}

    ///////////////////////////////////////  COLLISION HANDLING  //////////////////////////////////////////////////////////////////////////////

    void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            PlatformScript platform = coll.gameObject.GetComponent<PlatformScript>();
            //Change player localState to onGround if one contactPoint is on top of platform
            foreach (ContactPoint2D cp2d in coll.contacts)
            {
                if (platform.GetGround(cp2d.normal))
                {
                    local = PlayerState.Local.onGround;
                    break;
                }
            }            
        }

        if (coll.gameObject.tag == "Shadow" && (Input.GetKeyDown(KeyCode.S)))
        {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 1.0f, gameObject.transform.localScale.z);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 2.3f, gameObject.transform.localScale.z);
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground" || coll.gameObject.tag == "Default")
        {
            local = Local.inAir;
        }
    }


    ///////////////////////////////////////  GAME STATE HANDLING  //////////////////////////////////////////////////////////////////////////////

	public void SetMotionClimb()
	{
		Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Ground"),true);
		rb2d.bodyType = RigidbodyType2D.Kinematic;
		playerState.motion = PlayerState.Motion.climb;
	}
	public void SetMotionWalk()
	{
		Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Ground"),false);
		rb2d.bodyType = RigidbodyType2D.Dynamic;
		playerState.motion = PlayerState.Motion.walk;
	}
    public void Win()
    {
        GameObject.Find("GUI").GetComponent<GUIManager>().derText.color = Color.green;
        GameObject.Find("GUI").GetComponent<GUIManager>().textString = "YOU WIN";
    }

    public void Die()
    {
        GameObject.Find("GUI").GetComponent<GUIManager>().derText.color = Color.red;
        GameObject.Find("GUI").GetComponent<GUIManager>().textString = "YOU DIED";
        Destroy(this.gameObject);
    }


}
