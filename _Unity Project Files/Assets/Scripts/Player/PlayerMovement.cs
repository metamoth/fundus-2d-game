﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    //TOOLTIPS FOR ALL PROTOTYPING VARIABLES!!!
    //KEEP IT PRETTY, girls
   
    [Header ("Lateral Motion")]
	public float walkSpeed = 1.0f;
    [Space(5)]
    public float crouchSpeed = 0.5f;
    public float crouchCoolDown = 0.5f;

    [Header("Jumping")]
    public bool enableJump;
    public float jumpForce = 170.0f;
    public float jumpCoolDown = 0.2f;

    [Header("Analysis")]
    public bool showDebugWireFrame = false;

    [Header("Experimental - probaply broken")]
    public bool alignWithGround = false;
    public bool slide = false;
    public float slideSpeed;



    //Utility
    private LayerMask layerMask;
    private PlayerState playerState;
    private Rigidbody2D rb2d;
    private float currentSpeed;
    private float jumpCD;
    private float crouchCD;
    private List<Vector3> boxVert = new List<Vector3>();
    private float offsetScale = 0.06666f;
    private float rise;
    private bool jumpKeyWasUp = true;
	private bool touchLadder = false;



	private bool stillOnStairs = false;

    private Vector3 Rotate2DVector(Vector2 vector, float a)
    {
        a *= Mathf.Deg2Rad;
        float x = Mathf.Cos(a) * vector.x - Mathf.Sin(a) * vector.y;
        float y = Mathf.Sin(a) * vector.x + Mathf.Cos(a) * vector.y;
        return new Vector3(x, y,0);
    }



    // Use this for initialization
    void Start () {

        rb2d = this.gameObject.GetComponent<Rigidbody2D>();
        currentSpeed = walkSpeed;
        jumpCD = jumpCoolDown;
        crouchCD = crouchCoolDown;
        layerMask = LayerMask.GetMask("Ground");

        playerState = gameObject.GetComponent<PlayerState>();

        boxVert.Add(Rotate2DVector(new Vector3(-offsetScale * transform.localScale.x,  offsetScale * transform.localScale.y, 0), transform.eulerAngles.z));
        boxVert.Add(Rotate2DVector(new Vector3( offsetScale * transform.localScale.x,  offsetScale * transform.localScale.y, 0), transform.eulerAngles.z));
        boxVert.Add(Rotate2DVector(new Vector3(-offsetScale * transform.localScale.x, -offsetScale * transform.localScale.y, 0), transform.eulerAngles.z));
        boxVert.Add(Rotate2DVector(new Vector3( offsetScale * transform.localScale.x, -offsetScale * transform.localScale.y, 0), transform.eulerAngles.z));
    }
	
	// Update is called once per frame
	void Update () {
        
    }


    //ALWAYS USE PHYSICS!!!
    void FixedUpdate()
    {
        //Update CDs
        //JUMP
        if (jumpCD > 0)
        {
            jumpCD -= Time.deltaTime;
        }
        else if (jumpCD < 0)
        {
            jumpCD = 0;
        }
        //CROUCH
        if (crouchCD > 0)
        {
            crouchCD -= Time.deltaTime;
        }
        else if (crouchCD < 0)
        {
            crouchCD = 0;
        }

        ///////////////////////////GENERAL MOTION///////////////////////////////////////////////////////////////////////

        //Walk//////////////////////////////////////////

        switch (playerState.motion)
        {

            case PlayerState.Motion.walk:
                //ALWAYS
                if (Input.GetAxis("Horizontal") < -.1)
                {
                    rb2d.velocity = new Vector2(-currentSpeed, rb2d.velocity.y);
                    playerState.direction = -1;
                }
                else if (Input.GetAxis("Horizontal") > .1)
                {
                    rb2d.velocity = new Vector2(currentSpeed, rb2d.velocity.y);
                    playerState.direction = 1;
                }
                else
                {
                    rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                }

                //ONLY ON GROUND
                if (playerState.local == PlayerState.Local.onGround && enableJump)
                {
                    if (Input.GetAxis("Jump") == 1)
                    {
                        rb2d.AddForce(Vector2.up * jumpForce);
                    }
                }
                break;

            case PlayerState.Motion.climb:
                if (Input.GetAxis("Vertical") < -.1)
                {
                    rb2d.velocity = new Vector2(0, -currentSpeed);
                }
                else if (Input.GetAxis("Vertical") > .1)
                {
                    rb2d.velocity = new Vector2(0, currentSpeed);
                }
                else
                {
                    rb2d.velocity = new Vector2(0, 0);
                }
                break;
                /////////ANALYSIS////////////////////////////////////////////////////////////////////////
                if (showDebugWireFrame)
                {
                    boxVert[0] = Rotate2DVector(new Vector3(-offsetScale * transform.localScale.x, offsetScale * transform.localScale.y, 0), transform.eulerAngles.z);
                    boxVert[1] = Rotate2DVector(new Vector3(offsetScale * transform.localScale.x, offsetScale * transform.localScale.y, 0), transform.eulerAngles.z);
                    boxVert[2] = Rotate2DVector(new Vector3(-offsetScale * transform.localScale.x, -offsetScale * transform.localScale.y, 0), transform.eulerAngles.z);
                    boxVert[3] = Rotate2DVector(new Vector3(offsetScale * transform.localScale.x, -offsetScale * transform.localScale.y, 0), transform.eulerAngles.z);

                    foreach (Vector3 vert in boxVert)
                    {

                        Debug.DrawLine(transform.position, transform.position + vert);
                    }
                }
        }





        //GENERAL PLAYER MOVEMENT [left,right,jump,crouch]
        //void Crouch()
        //{
        //    if(Input.GetAxis("Crouch") >= .5 && crouchCD == 0 && playerState.motion != PlayerState.Motion.crouch)
        //    {
        //        //Scale player to crouch size and reposition him
        //        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 1.0f, gameObject.transform.localScale.z);
        //        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.05f, gameObject.transform.position.z);

        //        //Change currentSpeed to crouch speed
        //        currentSpeed = crouchSpeed;

        //        //Set motion state and CD
        //        playerState.motion = PlayerState.Motion.crouch;
        //        crouchCD = crouchCoolDown;
        //    }
        //    else if (Input.GetAxis("Crouch") <= .1  && crouchCD == 0 && playerState.motion != PlayerState.Motion.crouch)
        //    {
        //        //Scale player to walk size
        //        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 2.3f, gameObject.transform.localScale.z);
        //        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.05f, gameObject.transform.position.z);

        //        //Change currentSpeed to walk speed
        //        currentSpeed = walkSpeed;

        //        //Set motion state
        //        playerState.motion = PlayerState.Motion.crouch;
        //    }
        //}



    }





	void OnCollisionStay2D(Collision2D coll)
	{
	
		if(coll.gameObject.tag == "Stairs")
		{
			stillOnStairs = true;
		}


	}

	void OnCollisionExit2D(Collision2D coll)
	{

		if(coll.gameObject.tag == "Stairs")
		{
			stillOnStairs = false;
			coll.gameObject.transform.parent.BroadcastMessage ("DeactivateCollider");

		}


	}


	void OnTriggerStay2D(Collider2D coll)
	{

		if (coll.gameObject.name == "EntryPointBottom" && Input.GetAxis ("Vertical") > 0.0f) {

			coll.gameObject.transform.parent.BroadcastMessage ("ActivateCollider");

		}
		else if (coll.gameObject.name == "EntryPointTop" && Input.GetAxis ("Vertical") < 0.0f) {

			coll.gameObject.transform.parent.BroadcastMessage ("ActivateCollider");

		}

//		if (coll.gameObject.name == "EntryPointBottom" && Input.GetAxis ("Vertical") < 0.0f) {
//
//			coll.gameObject.transform.parent.BroadcastMessage ("DeactivateCollider");
//
//		}
//		else if (coll.gameObject.name == "EntryPointTop" && Input.GetAxis ("Vertical") > 0.0f) {
//
//			coll.gameObject.transform.parent.BroadcastMessage ("DeactivateCollider");
//
//		}


	}

	void OnTriggerExit2D(Collider2D coll)
	{


		if (coll.gameObject.name == "EntryPointBottom" && Input.GetAxis ("Vertical") == 0.0f && stillOnStairs == false) {

		coll.gameObject.transform.parent.BroadcastMessage ("DeactivateCollider");

	}
		else if (coll.gameObject.name == "EntryPointTop" && Input.GetAxis ("Vertical") == 0.0f && stillOnStairs == false) {

		coll.gameObject.transform.parent.BroadcastMessage ("DeactivateCollider");

	}


	}

}

