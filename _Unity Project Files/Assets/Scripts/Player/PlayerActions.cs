﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {



	//Zum Überprüfen, ob die Taste schon auf an geschalten wurde.
	//Beim Einbau von Lichtschalter ändern! 
	private bool lightKeyUp = true;
    private PlayerState playerState;

    public Color colInteract;
    public Color colRegular;
    public bool grab = false;
    public Vector2 carryOffset = new Vector2(0, 0.3f);
    public Vector2 placeOffset = new Vector2(0, 0.25f);
	private float interactionCoolDownTime = 0.2f;


    public List<GameObject> pickUpsInReach;
	public List<GameObject> interactorsInReach;
	public GameObject pickUp;
    public GameObject interactor;
    private bool interactButtonUp = true;
	private float interactionCoolDown;
    private GameObject hintTex;

    private string[] interactors = new string[] {"Ladder"};

	private string[] pickUps = new string[] {"Light_Bulb", "Electro_Plug", "PickUp" };
    


    // Use this for initialization
    void Start () {
		interactionCoolDown = interactionCoolDownTime;
        playerState = GameObject.Find("Player").GetComponent<PlayerState>();
        pickUpsInReach = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        //INTERACTION STATES
        bool thereArePickUps = (pickUpsInReach.Count > 0);
        bool thereAreInteractors = (interactorsInReach.Count > 0);


        //PLAYER REACTION
        GetComponent<SpriteRenderer>().color =  (thereArePickUps || thereAreInteractors) ? colInteract : colRegular;

        //PLAYER ITEMS
        if (pickUp != null)
        {
            pickUp.GetComponent<Transform>().position = (Vector2)this.transform.position + carryOffset;
        }

        //PLAYER INTERACTION COOLDOWN
        if (interactionCoolDown > 0)
			interactionCoolDown -= Time.deltaTime;
		else if(interactionCoolDown < 0) {
			interactionCoolDown = 0;
		}

        //PLAYER INPUT BUTTON REPRESS
		if (Input.GetAxis ("Interact") == 0)
			interactButtonUp = true;
		
        //PLAYER MOTION STATES
		switch (playerState.motion) 
		{
            //WALKING
		    case PlayerState.Motion.walk:

                if (interactor == null)
                {
                    if (Mathf.Abs(Input.GetAxis("Vertical")) > 0 && interactionCoolDown == 0)
                    {
                        if (thereAreInteractors)
                        {
                            Interact(interactorsInReach[interactorsInReach.Count - 1]);
                        }
                    }

                }


                if (Input.GetAxis ("Interact") == 1 && interactButtonUp && interactionCoolDown == 0)
                {       
				    interactButtonUp = false;
				    interactionCoolDown = interactionCoolDownTime;

 
                    if (pickUp != null)
                    {
                        DropItem();
                    }
                    else if (thereArePickUps)
                    {
					    PickUpItem (GetPriorityItem());
				    }

			    }
				break;

            //CLIMBING
            case PlayerState.Motion.climb:

				if (Input.GetAxis ("Interact") == 1 && interactButtonUp  && interactionCoolDown == 0) 
				{
                    StopInteraction();

                }
                break;
		}


   	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + new Vector2(placeOffset.x, placeOffset.y));
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + carryOffset);
    }


    //////////////////////////////// ACTIONS ///////////////////////////////////////////////////////////////////////

    public GameObject GetPriorityItem()
    {
        for(int i = pickUpsInReach.Count-1; i >= 0; i--)
        {
            if(pickUpsInReach[i].GetComponent<ElectroPlugController>() != null)
            {
                if(pickUpsInReach[i].GetComponent<ElectroPlugController>().GetSocket() == null)
                {
                    return pickUpsInReach[i];
                }
            }
        }

        for (int i = pickUpsInReach.Count - 1; i >= 0; i--)
        {
            if (pickUpsInReach[i].GetComponent<ElectroPlugController>() != null)
            {
                return pickUpsInReach[i];
            }
        }

        for (int i = pickUpsInReach.Count - 1; i >= 0; i--)
        {
            return pickUpsInReach[i];
        }

        for (int i = interactorsInReach.Count - 1; i >= 0; i--)
        {
            return interactorsInReach[i];
        }
        return null;
    }

    public void Interact(GameObject target)
    {
        target.SendMessage("PlayerInteraction", playerState);
        interactor = target;
        interactorsInReach.Remove(interactor);
    }

    public void StopInteraction()
    {
        interactor.SendMessage("StopPlayerInteraction", playerState);
        interactor = null;
    }

    public void PickUpItem(GameObject target)
    {
        target.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        pickUp = target;
        pickUpsInReach.Remove(pickUp);

        BulbController bulbCon = pickUp.GetComponent<BulbController>();
        if (bulbCon != null)
        {
            if (bulbCon.GetSocket() != null)
            {
                bulbCon.GetSocket().SendMessage("PullOut");
                bulbCon.SetSocket(null);
                return;
            }
        }

        ElectroPlugController plugCon = pickUp.GetComponent<ElectroPlugController>();
        if (plugCon != null)
        {
            if(plugCon.GetSocket() != null)
            {
                plugCon.GetSocket().GetComponent<ElectroSocketScript>().PullOut();
                plugCon.SetSocket(null);
                return;
            }
        }
    }

    public void DropItem()
    {
        pickUp.transform.position = (Vector2)transform.position + new Vector2(placeOffset.x * playerState.direction, placeOffset.y);
        pickUp.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        pickUp = null;
    }



    //////////////////////////////// TRIGGERS ///////////////////////////////////////////////////////////////////////

    void OnTriggerEnter2D(Collider2D coll)
    {

        //Exceptions
        if (coll.transform.parent.gameObject == pickUp) return;
        if (coll.gameObject == interactor) return;



        //Interactors
        for (int i = 0; i < interactors.Length; i++)
        {
            if (coll.gameObject.tag == interactors[i])
            {
                interactorsInReach.Add(coll.gameObject);
                return;
            }
        }

        //Sockets
        if (coll.gameObject.tag == "Light_Socket")
        {
            if (coll.gameObject.GetComponent<LightSocketScript>().bulbInside != null)
            {
                pickUpsInReach.Add(coll.gameObject.GetComponent<LightSocketScript>().bulbInside);
                return;
            }

        }
        if (coll.gameObject.tag == "Electro_Socket")
        {
            if (coll.gameObject.GetComponent<ElectroSocketScript>().plugInside != null)
            {
                pickUpsInReach.Add(coll.gameObject.GetComponent<ElectroSocketScript>().plugInside);
                return;
            }
        }
            
        //PickUps with triggerChild
        if (coll.transform.parent != null && coll.transform.parent != pickUp)
        {
            //Check if parentTag of triggerChild is in tagList
            for (int i = 0; i < pickUps.Length-1; i++)
            {
                if (coll.transform.parent.gameObject.tag == pickUps[i])
                {
                    pickUpsInReach.Add(coll.transform.parent.gameObject);
                    return;
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        //Exceptions
        if (coll.transform.parent.gameObject == pickUp) return;
        if (coll.gameObject == interactor)
        {
            StopInteraction();
            return;
        }


        //Interactors
        for (int i = 0; i < interactors.Length; i++)
        {
            if (coll.gameObject.tag == interactors[i])
            {
                interactorsInReach.Remove(coll.gameObject);
                return;
            }
        }

        //Sockets
        if (coll.gameObject.tag == "Light_Socket")
        {
            if (coll.gameObject.GetComponent<LightSocketScript>().bulbInside != null)
            {
                pickUpsInReach.Remove(coll.gameObject.GetComponent<LightSocketScript>().bulbInside);
            }

        }
        else if (coll.gameObject.tag == "Electro_Socket")
        {
            if (coll.gameObject.GetComponent<ElectroSocketScript>().plugInside != null)
            {
                pickUpsInReach.Remove(coll.gameObject.GetComponent<ElectroSocketScript>().plugInside);
            }
        }

        //PickUps with triggerChild
        if (coll.transform.parent != null)
        {
            //Check if parentTag of triggerChild is in tagList
            for (int i = 0; i < pickUps.Length; i++)
            {
                if (coll.transform.parent.gameObject.tag == pickUps[i])
                {
                    pickUpsInReach.Remove(coll.transform.parent.gameObject);
                    return;
                }
            }
        }
        //Interactors with directTrigger
            //Check if tag of triggerObject is in tagList
            
        
    }

}
