﻿using UnityEngine;
using System.Collections;

public class PixelCollider : MonoBehaviour
{

    public Texture2D collisionMask;
    public bool pixelCollision;

    void Awake()
    {
        if (collisionMask == null)
        {
            collisionMask = GetComponent<SpriteRenderer>().sprite.texture;
        }
    }

    public bool CollidesWith(PixelCollider other)
    {
        if (pixelCollision)
        {
            return  PerPixelCollision.Collides(this, other);
        }
        return GetBounds().Overlaps(other.GetBounds());
    }

    public Bounds2D GetBounds()
    {
        return new Bounds2D(
            (int)transform.position.x - collisionMask.width * (int)transform.localScale.x / 2,
            (int)transform.position.y - collisionMask.height * (int)transform.localScale.y / 2,
            collisionMask.width * (int)transform.localScale.x,
            collisionMask.height * (int)transform.localScale.y);
    }
}
