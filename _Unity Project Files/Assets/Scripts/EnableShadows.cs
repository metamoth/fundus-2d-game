﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableShadows : MonoBehaviour {



	private SpriteRenderer derRenderer;
	// Use this for initialization
	void Start () {

		derRenderer = this.GetComponent<SpriteRenderer> ();

		derRenderer.receiveShadows = true;
		derRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
