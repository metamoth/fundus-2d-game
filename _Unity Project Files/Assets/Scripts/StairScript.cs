﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ActivateCollider()
	{
		this.GetComponent<BoxCollider2D> ().enabled = true;
	}

	void DeactivateCollider()
	{
		this.GetComponent<BoxCollider2D> ().enabled = false;
	}
}
