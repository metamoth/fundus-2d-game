﻿using UnityEngine;
using System.Collections;

public class Bounds2D
{

    public int X { get; set; }
    public int Y { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }

    public Bounds2D() : this(0, 0, 0, 0) { }

    public Bounds2D(int X, int y, int Width, int Height)
    {
        this.X = X;
        this.Y = Y;
        this.Width = Width;
        this.Height = Height;
    }

    public bool Overlaps(Bounds2D other)
    {
        return IsHorizontalOverlap(other) && IsVerticalOverlap(other);
    }

    public bool IsHorizontalOverlap(Bounds2D other)
    {
        return !(X > other.X + other.Width || other.X > X + Width);
    }

    public bool IsVerticalOverlap(Bounds2D other)
    {
        return !(Y > other.Y + other.Height || other.Y > Y + Height);
    }

    public Bounds2D Intersection(Bounds2D other)
    {
        Bounds2D ret = new Bounds2D();
        ret.X = Mathf.Max(X, other.X);
        ret.Y = Mathf.Max(Y, other.Y);
        ret.Width = Mathf.Abs(ret.X - Mathf.Min(X + Width, other.X + other.Width));
        ret.Height = Mathf.Abs(ret.Y - Mathf.Min(Y + Height, other.Y + other.Height));

        return ret;
    }

    public Bounds2D ToLocal(Bounds2D global)
    {
        Bounds2D ret = new Bounds2D();
        ret.X = global.X - X;
        ret.Y = global.Y - Y;
        ret.Width = global.Width;
        ret.Height = global.Height;
        return ret;
    }

    public override string ToString()
    {
        return "(" + X + "," + Y + "," + Width + "," + Height + ")";
    }

}