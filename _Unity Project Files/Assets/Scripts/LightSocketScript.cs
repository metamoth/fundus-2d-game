﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSocketScript : MonoBehaviour
{

    public GameObject bulbInside = null;
    public Vector2 bulbOffset;
    public float coolDownTime;
    public bool global;

    private WhiteGuyScript whiteGuyScript;
    private PlayerActions playerActions;
    private PlayerState playerState;
    private LightController Lights;
    private PolygonCollider2D lightArea;
    private float coolDown = 0;
    private Transform dynEnv;

    // Use this for initialization
    void Start()
    {
        playerActions = GameObject.Find("Player").GetComponent<PlayerActions>();
        playerState = GameObject.Find("Player").GetComponent<PlayerState>();
        //        whiteGuyScript = GameObject.Find("WhiteGuy").GetComponent<WhiteGuyScript>();
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        dynEnv = GameObject.Find("Dynamic Environment").transform;
    }

    void Update()
    {
        if (coolDown > 0)
        {
            coolDown -= Time.deltaTime;
        }
        else if (coolDown < 0)
        {
            coolDown = 0;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + bulbOffset);
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if (bulbInside == null)
            if (coll.gameObject.tag == "Light_Bulb")
                if (coll.gameObject.GetComponent<Rigidbody2D>() != null)
                    if (coll.gameObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
                        PutInsideSocket(coll.gameObject);
    }

    //void OnGUI()
    //{
    //    Rect screenRect =  new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y)
    //    GUI.Label(screenRect, "Text");
    //}

    private void PutInsideSocket(GameObject bulb)
    {
        //bulb physics
        bulb.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        bulb.transform.position = (Vector2)transform.position + bulbOffset;

        //bulb exchange
        playerActions.pickUp = null;
        bulbInside = bulb;
        bulbInside.GetComponent<BulbController>().SetSocket(gameObject);

        Lights.Power();

    }

    //void PullOutsideSocket()
    //{   //bulb        
    //    bulbInside.GetComponent<BulbController>().TurnOff();

    //    //bulb exchange
    //    playerActions.PickUpItem(bulbInside);
    //    bulbInside = null;


    //    //socket
    //    lightSource.GetComponent<PolygonCollider2D>().enabled = true;
    //    gameObject.GetComponent<CircleCollider2D>().enabled = false;

    //}
    public void Power(bool power)
    {
        if (power)
        {
            TurnOn();
        }
        else
        {
            TurnOff();
        }
    }

    public void TurnOn()
    {
        //ALL CHANGES DEPEND IN LIGHTBULB INSIDE
        if (bulbInside != null)
        {
            bulbInside.GetComponent<BulbController>().TurnOn();
        }
    }

    public void TurnOff()
    {

        //NO CHANGES DEPEND IN LIGHTBULB INSIDE
        if (bulbInside != null)
        {
            bulbInside.GetComponent<BulbController>().TurnOff();
        }

        //Environment change
        for (int i = 0; i < dynEnv.childCount; i++)
        {
            GameObject box = dynEnv.GetChild(i).gameObject;
            EnvSwitchScript boxScript = box.GetComponent<EnvSwitchScript>();
            box.SetActive(lightArea.OverlapPoint((Vector2)playerState.transform.position) && boxScript.visibileInDark);
        }

        playerState.inLightArea = false;
        //whiteGuyScript.inLightArea = false;

    }

    public void PullOut()
    {
        TurnOff();
        bulbInside = null;
    }
}
