﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BasicPlatform : MonoBehaviour {

    [Header("Platform Options")]
    public Vector3 initPlatformOffset;
    public float liftSpeed = 1f;
    public float offsetScale = 2.0f;

    [Header("Position Options")]
    public Vector3 initPosition;
    public float moveLeft = 1.0f;
    public float moveRight = -1.0f;
    public float moveSpeed = 50f;
    

    private float dir = 1f;
    private Transform platformBox;
    private LightController Lights;
    private Rigidbody2D rb2d;
    private bool powered = false;

	// Use this for initialization
	void Start () {
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        platformBox = transform.Find("PlatformBox");
        initPlatformOffset = platformBox.localPosition;
        initPosition = transform.position;
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        //COLOR
        if (powered)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 0, 1);
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1);
        }

        //MOTION
        if (!powered)
        {
            rb2d.velocity = new Vector2(0, 0);

            if (platformBox.localPosition.y > initPlatformOffset.y)
            {
                platformBox.localPosition += (initPlatformOffset - platformBox.localPosition) * liftSpeed * Time.deltaTime;
            }
            else
            {
                platformBox.localPosition = initPlatformOffset;
            }
            
        }
        else
        {
            rb2d.velocity = new Vector2(dir * moveSpeed * Time.deltaTime, 0);

            if (platformBox.localPosition.y < initPlatformOffset.y * offsetScale)
            {
                platformBox.localPosition -= (platformBox.localPosition - initPlatformOffset * offsetScale) * liftSpeed * Time.deltaTime;
            }
            else
            {
                platformBox.localPosition = initPlatformOffset * offsetScale;
            }
        }
        if (transform.position.x >= initPosition.x - moveLeft)
        {
            if (dir != -1.0f)
            {
                dir = -1.0f;
                transform.position += new Vector3(dir * moveSpeed * .001f, 0, 0);
            }
            
        } else if(transform.position.x <= initPosition.x + moveRight)
        {
            if (dir != 1.0f)
            {
                dir = 1.0f;
                transform.position += new Vector3(dir * moveSpeed * .001f, 0, 0);
            }
            transform.position += new Vector3(dir * moveSpeed * .001f, 0, 0);
        }

        
	}

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.transform.tag == "Ground")
        {
            dir *= -1;
            transform.position += new Vector3(dir * moveSpeed * .001f, 0,0);
        }
    }
    public void Power(bool power)
    {
        this.powered = power;
    }
}
