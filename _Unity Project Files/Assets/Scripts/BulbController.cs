﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbController : MonoBehaviour {

    public Color LightOnColor;
    public Color LightOffColor;
    public int rays = 128;
    public LayerMask layerMask;
    public bool isOn = false;

    public float rayLength = 1f;
    public float rayVisibility = 0.075f;
    public float sampleUnit = 1f;

    private GameObject socket;
    private GameObject lightFX;



    private SpriteRenderer spriteRenderer;
    

    //UNITY START
    private void Start()
    {
        lightFX = transform.Find("LightBulb_FX").gameObject;

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.color = LightOffColor;
    }
    //UNITY UPDATE
    private void Update()
    {
        if (isOn)
        {
            Light(rays);
        }
    }


    public GameObject GetSocket()
    {
        if (socket != null)
        {
            return this.socket;
        }
        else
        {
            return null;
        }
    }

    public void SetSocket(GameObject s)
    {
        this.socket = s;
    }
    public void TurnOn()
    {
        spriteRenderer.color = LightOnColor;
        isOn = true;
    }

    public void TurnOff()
    {
        spriteRenderer.color = LightOffColor;
        isOn = false;
    }

    void Light(int rays)
    {
        //Mesh Data
        Vector3[] vertecies = new Vector3[rays];
        int[] triangles = new int[(rays-2)*3];
        vertecies[0] = new Vector3(0,0,0);
        int vertexCount = 1;

        //Ray Data
        
        List<RaycastHit2D> hits = new List<RaycastHit2D>();

        for (int i = 1; i <= rays; i++)
        {
            float angle = 2 * Mathf.PI * ((float)i/ (float)rays);
            Vector2 rayDir = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
            RaycastHit2D hit = (Physics2D.Raycast(transform.position, rayDir, rayLength, layerMask));
            if (hit)
            {
                Debug.DrawLine(transform.position, hit.point, new Color(1, 1, 0, rayVisibility));
                
                

            }
            else
            {
                Debug.DrawLine(transform.position, (Vector2)transform.position + rayLength * rayDir, new Color(1, 1, 0, rayVisibility));
            }
            if(vertecies.Length == 0 || ((Vector2)vertecies[vertecies.Length-1] - hit.point).magnitude >= sampleUnit)
            {
                vertecies[vertexCount] = hit.point;
                vertexCount++;
            }
            hits.Add(hit);



        }

        Color pointColor = new Color(1, 1, 1, 1);
        int counter = 0;
        foreach (Vector3 vertex in vertecies)
        {
            counter++;
            Debug.DrawLine(vertex + new Vector3(.1f, .1f, 0), vertex + new Vector3(-.1f, -.1f, 0), new Color(1, 1, 1, counter / vertecies.Length));
            Debug.DrawLine(vertex + new Vector3(-.1f, .1f, 0), vertex + new Vector3(.1f, -.1f, 0), new Color(1, 1, 1, counter / vertecies.Length));
        }
        ////Generate tris
        //for (int i = 0; i < triangles.Length/3; i++)
        //{
        //    triangles[i*3] = 0;
        //    triangles[i*3+1] = i;
        //    triangles[i*3+2] = i+1;
        //}
        ////Create Mesh
        //Mesh mesh = new Mesh();
        //mesh.vertices = vertecies;
        //mesh.triangles = triangles;

        //lightFX.GetComponent<MeshFilter>().mesh = mesh;




    }


}
