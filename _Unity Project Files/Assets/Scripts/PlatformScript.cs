﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour {

    public enum Collision {none, wall, ground};

    [Header("Platform Collision")]
    public Collision top;
    public Collision right;
    public Collision bottom;
    public Collision left;

    [Header("Analysis")]
    public bool showCollisionFrame;
    public bool showCollisionPoints;
    public float normalLength = .1f;


    private GameObject Player;
    private LightController Lights;
    private SpiderTableBehavior EnemyCore;


    private Vector3 Rotate3(Vector3 vector, float a)
    {
        a *= Mathf.Deg2Rad;
        float x = Mathf.Cos(a) * vector.x - Mathf.Sin(a) * vector.y;
        float y = Mathf.Sin(a) * vector.x + Mathf.Cos(a) * vector.y;
        return new Vector3(x, y, 0);
    }


    private void OnDrawGizmos()
    {
        if (showCollisionFrame && gameObject.GetComponent<Renderer>().enabled)
        {
            Vector2 size = gameObject.GetComponent<BoxCollider2D>().size;
            Vector2 offset = gameObject.GetComponent<BoxCollider2D>().offset;

            Color enableColl  = new Color(1.0f, 1.0f, 0.3f, 1f);
            Color enableJump = new Color(0.5f, 1f, 1.0f, 1f);

            float rotation = transform.eulerAngles.z;

            if (top != Collision.none)
            {
                Gizmos.color = (top == Collision.ground) ? enableColl : enableJump;
                Vector3 start = transform.position + Rotate3(Vector3.Scale(new Vector3(-0.5f * size.x,  0.5f * size.y, 0), transform.localScale), rotation);
                Vector3 end   = transform.position + Rotate3(Vector3.Scale(new Vector3( 0.5f * size.x,  0.5f * size.y, 0), transform.localScale), rotation);
                Gizmos.DrawLine(start,end);
            }
            if (right != Collision.none)
            {
                Gizmos.color = (right == Collision.ground) ? enableColl : enableJump;
                Vector3 start = transform.position + Rotate3(Vector3.Scale(new Vector3(-0.5f * size.x, -0.5f * size.y, 0), transform.localScale), rotation);
                Vector3 end   = transform.position + Rotate3(Vector3.Scale(new Vector3(-0.5f * size.x,  0.5f * size.y, 0), transform.localScale), rotation);
                Gizmos.DrawLine(start, end);
            }
            if (bottom != Collision.none)
            {
                Gizmos.color = (bottom == Collision.ground) ? enableColl : enableJump;
                Vector3 start = transform.position + Rotate3(Vector3.Scale(new Vector3(-0.5f * size.x, -0.5f * size.y, 0), transform.localScale), rotation);
                Vector3 end   = transform.position + Rotate3(Vector3.Scale(new Vector3 (0.5f * size.x, -0.5f * size.y, 0), transform.localScale), rotation);
                Gizmos.DrawLine(start, end);
            }
            if (left != Collision.none)
            {
                Gizmos.color = (left == Collision.ground) ? enableColl : enableJump;
                Vector3 start = transform.position + Rotate3(Vector3.Scale(new Vector3(0.5f * size.x, 0.5f * size.y, 0), transform.localScale), rotation);
                Vector3 end = transform.position + Rotate3(Vector3.Scale(new Vector3(0.5f * size.x, -0.5f * size.y, 0), transform.localScale), rotation);
                Gizmos.DrawLine(start, end);
            }
        }
    }

    // Use this for initialization
    void Start () {
        Player = GameObject.Find("Player");
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
    }


    void OnCollisionStay2D(Collision2D coll)
    {
        if (showCollisionPoints)
        {
            foreach (ContactPoint2D contact in coll.contacts)
            {
                Debug.DrawLine(transform.position, contact.point, Color.white);
                Debug.DrawLine(contact.point, contact.point + Rotate(contact.normal, 180) * normalLength, Color.blue);
            }
        }
            
    }

    private Vector2 Rotate(Vector2 v, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(angle) * v.x - Mathf.Sin(angle) * v.y, Mathf.Sin(angle) * v.x + Mathf.Cos(angle) * v.y);
    }

    private Vector3 Rotate(Vector3 v, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(angle) * v.x - Mathf.Sin(angle) *v.y, Mathf.Sin(angle) * v.x + Mathf.Cos(angle) * v.y, 0);
    }


    public bool GetCollision(Vector2 normal)
    {
        float angle = transform.eulerAngles.z;
        if (Rotate(Vector2.up, angle) == normal) { return (top.Equals(Collision.wall)); }
        else if (Rotate(Vector2.right, angle) == normal) { return (right.Equals(Collision.wall)); }
        else if (Rotate(Vector2.down, angle) == normal) { return (bottom.Equals(Collision.wall)); }
        else if (Rotate(Vector2.left, angle) == normal) { return (left.Equals(Collision.wall)); }
        else
        {
            return false;
        }
    }

    public bool GetGround(Vector2 normal)
    {
        float angle = transform.eulerAngles.z;
        if (Rotate(Vector2.up, angle) == normal) { return (top.Equals(Collision.ground));}
        else if (Rotate(Vector2.right, angle) == normal) { return (right.Equals(Collision.ground)); }
        else if (Rotate(Vector2.down, angle) == normal) { return (bottom.Equals(Collision.ground)); }
        else if (Rotate(Vector2.left, angle) == normal) { return (left.Equals(Collision.ground)); }
        else
        {
            return false;
        }
    }

    public bool InCollisionZone(GameObject target)
    {

        return true;
    }
}
