﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteGuyScript : MonoBehaviour {


	[Tooltip("Die Geschwindigkeit beim Patroullieren")]
	public float walkSpeed = 1.0f;
	[Tooltip("Die Geschwindigkeit beim auf den Spieler zu rennen") ]
	public float dashSpeed = 5.0f;

    public float fieldOfView;
    [Tooltip("rays are only cast in this layer - select 'Player'")]
    public LayerMask layerMask1;
    public LayerMask layerMask2;

    public bool inLightArea = false;

    //-1 = links, 1 = rechts
    private int direction = 1;
	private RaycastHit2D hit;
    private Rigidbody2D rb2d;
    private GameObject Player;
    private LayerMask rayCastLayerMask;
    private LightController Lights;
    private bool visible = false;


	private GameObject[] telepoints;


    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, Rotate(direction * Vector2.right, fieldOfView));
        Gizmos.DrawLine(transform.position, Rotate(direction * Vector2.right, -fieldOfView));
    }

    // Use this for initialization
    void Start () {
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        Player = GameObject.Find("Player");
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        rayCastLayerMask = (layerMask1 | layerMask2);

		telepoints = GameObject.FindGameObjectsWithTag ("Telepoint");
    }
	
	// Update is called once per frame
	void Update () {
        
        
		//TELEPORT TEST.
		if (Input.GetKey (KeyCode.P)) {
            Teleport();
        }

        if (!visible && inLightArea)
        {
            visible = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            rb2d.bodyType = RigidbodyType2D.Dynamic;
            
            Teleport();
        }
        else if (visible && !inLightArea)
        {
            visible = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            rb2d.bodyType = RigidbodyType2D.Static;
        }

        if (visible)
        {
            Vector2 upperLimit = Rotate(new Vector2(direction, 0), fieldOfView / 2);
            Vector2 lowerLimit = Rotate(new Vector2(direction, 0), -fieldOfView / 2);
            Vector2 toPlayer = (Vector2)Player.transform.position - (Vector2)transform.position;

            Debug.DrawLine((Vector2)transform.position, (Vector2)transform.position + upperLimit * 10f, Color.red);
            Debug.DrawLine((Vector2)transform.position, (Vector2)transform.position + lowerLimit * 10f, Color.red);

            if (Vector2.Angle(new Vector2(direction, 0), toPlayer) < fieldOfView / 2f)
            {
                hit = Physics2D.Raycast(transform.position, toPlayer, float.PositiveInfinity, rayCastLayerMask);
                Debug.DrawLine((Vector2)transform.position, Player.transform.position, Color.red);
                //Wenn der Spieler gesehen wird, renne zu ihm, wenn nicht, patroulliere.
                if (hit.collider.gameObject == GameObject.Find("Player"))
                {
                    Dash();
                    return;
                }
            }
            Patrol();
            return;
        }
        
        
        
	}   

	void Patrol(){
        gameObject.GetComponent<SpriteRenderer>().color = new Color(150f / 255f, 62 / 255f, 62 / 255f, 1f);
        this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (direction * walkSpeed,0);
	
	}

	void Dash()
	{
        gameObject.GetComponent<SpriteRenderer>().color = new Color(250f / 255f, 0f, 0f, 1f);
        this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (direction * dashSpeed,0);
	}



	//Umdrehen, wenn er gegen eine Wand läuft
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Wall") {
		
			direction = -direction;	

		}
	}

    private Vector2 Rotate(Vector2 v, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(angle)*v.x - Mathf.Sin(angle)*v.y,Mathf.Sin(angle)*v.x + Mathf.Cos(angle)*v.y);
    }


	//Teleportiert den Whiteguy zu dem Telepoint, der am nächsten am Spieler liegt
	void Teleport()
	{
		float min = 0.0f;
		GameObject derNächste = telepoints[0];
		for(int i = 0; i < telepoints.Length; i++){
			
			float distanz = Vector2.Distance (telepoints[i].transform.position, GameObject.Find ("Player").transform.position);


			if (min == 0.0f) {
				min = distanz;
			}
			if (distanz < min) {
				min = distanz;
				derNächste = telepoints [i];
			
			}


		}

		this.transform.position = derNächste.transform.position;

	}

}


