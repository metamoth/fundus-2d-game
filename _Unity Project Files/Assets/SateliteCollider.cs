﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SateliteCollider : MonoBehaviour {

    public GameObject core;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D coll)
    {
        bool wall = false;
        foreach(ContactPoint2D cp2d in coll.contacts)
        {
            if (Vector2.Angle(Vector2.up, cp2d.normal) > 45)
            {
                wall = true;
            }
        }
        if (wall) core.GetComponent<SpiderTableBehavior>().SendMessage("Turn");
    }
}
